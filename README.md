# Docker Static Content

Serve static content using an [NGINX](https://hub.docker.com/_/nginx) webserver behind a [traefik](https://hub.docker.com/_/traefik) reverse proxy. 
This setup uses Docker Compose to deploy the service NGINX service. The traefik service (Version 2.X - we used 2.2) should already run and work properly. The NGINX service connects to traefik using the docker network `traefik-net`.
The content that should be served can be inserted in the directory `content`.


[![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Nginx_logo.svg/320px-Nginx_logo.svg.png)](https://hub.docker.com/_/nginx)
[![](https://d1q6f0aelx0por.cloudfront.net/product-logos/library-traefik-logo.png)](https://hub.docker.com/_/traefik)

## Setup

1. Ensure `traefik` runs correctly
2. Change the `www.example.com` in `docker-compose.yaml` to your domain.
3. Start Services
```shell
docker-compose up -d
```
